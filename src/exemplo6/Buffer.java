/**
 * Buffer
 * 
 * Autor: Willian Alberto Lauber
 * Ultima modificacao: timestamp
 */
package src.exemplo6;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
